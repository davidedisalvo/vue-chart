import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        pieChartValue1: 10,
        pieChartValue2: 30,
        pieChartValue3: 20,
        pieChartValue4: 30,
    },
    mutations: {
        setData1(state, n) {
            state.pieChartValue1 = n
        }
    },
    getters: {
        data1: state => {
            return state.pieChartValue1;
        },
        data2: state => {
            return state.pieChartValue2;
        },
        data3: state => {
            return state.pieChartValue3;
        },
        data4: state => {
            return state.pieChartValue4;
        }
    }
})